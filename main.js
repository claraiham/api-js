async function get() {
    const response = await fetch("https://jsonplaceholder.typicode.com/users"); // je récupère le lien json qui contient les données
    const characterList = await response.json(); // on donne le nom characterList au tableau récupéré

    for (const oneCharacter of characterList) {
        let name = oneCharacter.name; // je récupere le nom dns une variable
        let nameStreet = oneCharacter.address.street; // je récupere le nom de la rue dns une variable, contenu dans un objet nommé adress
        let code = oneCharacter.address.zipcode; // je récupere le code postal dns une variable, contenu dans un objet nommé adress
        let cityName = oneCharacter.address.city; // je récupere le nom de la ville dns une variable, contenu dans un objet nommé adress

        console.log(name, `street : ${nameStreet}`, `zipcode : ${code}`, `city : ${cityName}`);// j'affiche mes variables dans la console
    }
}

get(); // appel de la fonction